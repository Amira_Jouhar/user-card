import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, tap, throwError } from 'rxjs';
import { apiUrl } from 'src/app/common/config.app';

@Injectable({
  providedIn: 'root',
})
export class CardsService {
  constructor(private http: HttpClient) {}
  getCards(page: number): Observable<any> {
    let params = new HttpParams();
    params = params.append('page', page);
    return this.http.get(`${apiUrl}users`, { params });
  }
  getCardsDetails(id: number): Observable<any> {
    return this.http.get(`${apiUrl}users/${id}`);
  }
}

import { Injectable } from '@angular/core';
import * as sizeofObjectPkg from 'object-sizeof';
import * as _ from 'lodash';
const sizeofObject = sizeofObjectPkg.default;

@Injectable({
  providedIn: 'root',
})
export class CachingService {
  private readonly cache: Map<string, { value: any; timestamp: number }> =
    new Map<string, any>();
  private readonly maxRequestNumber = 50;
  private readonly maxSizeMb = 50;

  constructor() {}

  getValue(key: string): any | null | undefined {
    return _.cloneDeep(this.cache.get(key)?.value);
  }

  setValue(key: string, value: any) {
    if (this.shouldRemove()) {
      const oldKey = this.getOldestResponseKey();
      this.cache.delete(oldKey);
    }
    this.cache.set(key, { value: _.cloneDeep(value), timestamp: Date.now() });
  }

  hasValue(key: string): boolean {
    return this.cache.has(key);
  }

  private getOldestResponseKey(): string {
    return Array.from(this.cache.entries())
      .map((e) => ({ key: e[0], timestamp: e[1].timestamp }))
      .sort((a, b) => a.timestamp - b.timestamp)[0].key;
  }

  private shouldRemove(): boolean {
    return (
      this.cache.size >= this.maxRequestNumber &&
      sizeofObject(Array.from(this.cache.entries())) / 1e6 > this.maxSizeMb
    );
  }
}

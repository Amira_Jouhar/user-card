import { apiUrl } from '../../common/config.app';
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { CachingService } from './caching.service';
import { tap } from 'rxjs/operators';

@Injectable()
export class CachingInterceptor implements HttpInterceptor {
  constructor(private cachingService: CachingService) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    const key = this.buildKey(request);

    if (this.cachingService.hasValue(key)) {
      return of(this.cachingService.getValue(key));
    }

    return next.handle(request).pipe(
      tap((event) => {
        if (
          event instanceof HttpResponse &&
          event.status < 400 &&
          this.shouldCache(request)
        ) {
          this.cachingService.setValue(key, event);
        }
      })
    );
  }

  shouldCache(request: HttpRequest<unknown>): boolean {
    const base = apiUrl;

    const shouldStore: boolean =
      request.method.toLowerCase() == 'get' && request.url.includes(base);
    return shouldStore;
  }

  buildKey(request: HttpRequest<unknown>): string {
    return `${request.url}__${request.params.toString()}__${
      request.headers.has('Authorization')
        ? request.headers.get('Authorization')
        : ''
    }__${request.headers.has('lang') ? request.headers.get('lang') : ''}`;
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import {
  TranslateLoader,
  TranslateModule,
  TranslateService,
} from '@ngx-translate/core';
import { MaterialModule } from '../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CardComponent } from '../components/card/card.component';
import { RouterModule } from '@angular/router';
import { NavbarComponent } from '../components/navbar/navbar.component';
import { LoaderComponent } from '../components/loader/loader.component';
import { LottieModule } from 'ngx-lottie';
import { CardDetailsComponent } from '../components/card-details/card-details.component';
// AoT requires an exported function for factories
import player from 'lottie-web';
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
export function playerFactory() {
  return player;
}
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    LottieModule.forRoot({ player: playerFactory }),
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    NavbarComponent,
    CardComponent,
    LoaderComponent,
    CardDetailsComponent,
  ],
  exports: [
    NavbarComponent,
    CardComponent,
    TranslateModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    CardDetailsComponent,
    LoaderComponent,
    LottieModule,
  ],
})
export class SharedModule {
  constructor(translate: TranslateService) {
    const dir = localStorage.getItem('dir') || 'ltr';
    translate.setDefaultLang('en');
  }
}

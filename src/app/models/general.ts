export interface IPopupInput {
  imgUrl: string;
  message: string;
  rejectBtn: string;
  acceptBtn: string;
}
export interface IHasLoader {
  _showLoader: boolean;
  showLoader: () => void;
  hideLoader: () => void;
}

export interface IBidirectional {
  _dir: 'ltr' | 'rtl';
}

export class SharedParent implements IHasLoader, IBidirectional {
  _isRtl: boolean;
  _dir: 'ltr' | 'rtl';
  _showLoader: boolean;

  constructor() {
    this._showLoader = false;
    this._dir = (localStorage.getItem('dir') as any) || 'ltr';
    this._isRtl = this._dir == 'rtl';
  }

  showLoader = () => (this._showLoader = true);
  hideLoader = () => (this._showLoader = false);
}

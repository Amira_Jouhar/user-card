export class HasLoader {
  _showLoader = false;

  constructor() {}

  showLoader = () => (this._showLoader = true);
  hideLoader = () => (this._showLoader = false);
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/common/shared.module';
import { ItemsComponent } from './items/items.component';
import { CardsRoutingModule } from './cards-routing.module';
import { ItemDetailsComponent } from './item-details/item-details.component';

@NgModule({
  declarations: [ItemsComponent, ItemDetailsComponent],
  imports: [CommonModule, SharedModule, CardsRoutingModule],
})
export class CardsModule {}

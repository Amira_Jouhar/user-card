import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable, take } from 'rxjs';
import { SharedParent } from 'src/app/models/general';
import { CardsService } from '../../../services/api/cards.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-item-details',
  templateUrl: './item-details.component.html',
  styleUrls: ['./item-details.component.scss'],
})
export class ItemDetailsComponent extends SharedParent implements OnInit {
  _card: BehaviorSubject<any>;
  _card$: Observable<any>;
  id: any;

  constructor(
    public CardsService: CardsService,
    public rout: Router,
    public route: ActivatedRoute
  ) {
    super();
    this._card = new BehaviorSubject<any>(this.getInitialState());
    this._card$ = this._card?.asObservable();
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getCardsDetails(this.id);
  }
  getCardsDetails(id: any) {
    this.showLoader();
    this.CardsService.getCardsDetails(id)
      .pipe(take(1))
      .subscribe({
        next: (res: any) => this.getCardsDetailsSuccessed(res),
        error: (error) => this.httpRequestFailed(error),
        complete: () => {
          this.hideLoader();
        },
      });
  }
  getInitialState(): any {
    return {
      cards: [],
    };
  }
  getCardsDetailsSuccessed(res: any) {
    this._card.value.cards = this._card.value.cards.concat(res.data);
  }

  httpRequestFailed(httpError: any) {
    console.log(httpError);
    this.hideLoader();
  }
}

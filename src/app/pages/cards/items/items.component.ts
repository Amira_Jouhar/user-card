import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject, Observable, take } from 'rxjs';
import { SharedParent } from 'src/app/models/general';
import { CardsService } from '../../../services/api/cards.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss'],
})
export class ItemsComponent extends SharedParent implements OnInit {
  _card: BehaviorSubject<any>;
  _card$: Observable<any>;
  searchchar!: '';
  isResult: boolean = false;
  @ViewChild('searchTerm')
  searchBar!: ElementRef<HTMLInputElement>;
  constructor(public CardsService: CardsService, public rout: Router) {
    super();
    this._card = new BehaviorSubject<any>(this.getInitialState());
    this._card$ = this._card?.asObservable();
  }

  ngOnInit(): void {
    this.getCards();
  }
  getCards() {
    this.showLoader();
    this.changeState({ pageState: true });
    console.log('fffffff', this._card.value.page);
    this.CardsService.getCards(this._card.value.page + 1).subscribe({
      next: (res: any) => this.getCardsSuccessed(res),
      error: (error) => this.httpRequestFailed(error),
      complete: () => {
        this.hideLoader();
      },
    });
  }
  getInitialState(): any {
    return {
      page: 0,
      limit: 10,
      totalPages: 1,
      totalItems: -1,
      cards: [],
    };
  }
  getCardsSuccessed(res: any) {
    const cards = this._card.value.cards.concat(res.data);
    const page = this._card.value.page + 1;
    const total = res.total_pages;
    this.changeState(
      { cards, page, totalPages: total, pageState: false },
      'getcards'
    );
  }
  changeState(changes: Partial<any>, source: string = '') {
    if (source != '') console.log('changes source:', source, changes);
    this._card.next({ ...this._card.value, ...changes });
  }
  httpRequestFailed(httpError: any) {
    console.log(httpError);
    this.hideLoader();
  }
  onScroll(e: Event) {
    const elem = e.target as HTMLDivElement;
    if (
      this._card.value.pageState ||
      this._card.value.page >= this._card.value.totalPages ||
      elem?.offsetHeight + elem.scrollTop < elem.scrollHeight - 5
    ) {
      return;
    }

    this.changeState({ pageState: true }, 'onScroll');
    this.getCards();
  }

  showDetails(id: any) {
    this.rout.navigate(['card/details/' + id]);
  }

  getCharactersSearch(value: any) {
    this.searchchar = value.trim();
    this.searchBar.nativeElement.focus();
    if (this.searchchar != '') {
      const result = this._card.value.cards.find(
        (x: any) => x.id == this.searchchar
      );
      if (result) {
        this.rout.navigate(['card/details/' + this.searchchar]);
        this.isResult = false;
      } else {
        this.isResult = true;
      }
    }
  }
  ClearSearch(value: any) {
    if (value === '' && this.isResult === true) {
      this.isResult = false;
    }
  }
}

import {
  Component,
  ElementRef,
  Input,
  OnInit,
  ViewChild,
  AfterViewInit,
} from '@angular/core';
import { SharedParent } from 'src/app/models/general';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent
  extends SharedParent
  implements OnInit, AfterViewInit
{
  @Input('selected') selected = false;
  @Input('img') img: string | undefined;
  @Input('first_name') first_name: string | undefined;
  @Input('last_name') last_name: string | undefined;
  @Input('id') id: number | undefined;

  @ViewChild('cardImg')
  cardImgDiv!: ElementRef<HTMLDivElement>;
  constructor() {
    super();
  }

  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.cardImgDiv.nativeElement.style.backgroundImage = `url(${
      this.img || './assets/images/card-default.png'
    })`;
  }
}

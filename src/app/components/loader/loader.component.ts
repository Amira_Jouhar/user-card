import { Component, OnInit } from '@angular/core';
import { AnimationOptions } from 'ngx-lottie';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss'],
})
export class LoaderComponent implements OnInit {
  lottieOptions: AnimationOptions = {
    path: './assets/lottie/Loader.json',
  };

  constructor() {}

  ngOnInit(): void {}
}
